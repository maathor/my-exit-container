FROM stakater/base-alpine:3.6
ENV EXIT_CODE=1
ADD start.sh /start.sh
CMD ["bash", "-c", "/start.sh"]
